import config from '../config';
import EventEmitter from 'eventemitter3';

const EVENTS = {
  APP_READY: 'app_ready',
};

/**
 * App entry point.
 * All configurations are described in src/config.js
 */
export default class Application extends EventEmitter {
  constructor() {
    super();

    this.config = config;
    this.data = {
      count: 0,
      planets: null,
    }
    this.getData()
  }

  static get events() {
    return EVENTS;
  }

  /**
   * Initializes the app.
   * Called when the DOM has loaded. You can initiate your custom classes here
   * and manipulate the DOM tree. Task data should be assigned to Application.data.
   * The APP_READY event should be emitted at the end of this method.
   */
  async getapi(url) {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  }
  async getData() {
    let api_url = "https://swapi.boom.dev/api/planets/"
    let api_res = await this.getapi(api_url)
    let planets = [];
    this.data.count = api_res.count
    for (let i = 1; i <= this.data.count; i++) {
      let planet = await this.getapi(api_url + i)
      planets.push(planet);
    }
    this.data.planets = planets;
    this.init()
  }

  async init() {
    this.emit(Application.events.APP_READY);
    // Initiate classes and wait for async operations here.
  }
}

